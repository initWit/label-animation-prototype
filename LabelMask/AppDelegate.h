//
//  AppDelegate.h
//  LabelMask
//
//  Created by Robert Figueras on 4/1/16.
//  Copyright © 2016 Robert Figueras. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

