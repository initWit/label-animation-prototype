//
//  ViewController.m
//  LabelMask
//
//  Created by Robert Figueras on 4/1/16.
//  Copyright © 2016 Robert Figueras. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *labelToMove;
@property (strong, nonatomic) NSMutableArray *labelsArray;
@property (weak, nonatomic) IBOutlet UIStackView *myHorStackView;
@property (strong, nonatomic) NSMutableArray *myCopyLabels;
@property BOOL isUp;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.labelsArray = [NSMutableArray new];
    self.myCopyLabels = [NSMutableArray new];
    self.isUp = YES;
    
    [self createLabelsForString:@"HI JESSICA,"];

//    CGFloat stackViewWidth;
//    for (UILabel *eachLabel in self.labelsArray) {
//        NSLog(@"WIDTH OF %@ = %g", eachLabel.text, eachLabel.frame.size.width);
//        stackViewWidth += eachLabel.frame.size.width;
//    }
    
//    UIStackView *spacerStackView = [[UIStackView alloc] initWithFrame:CGRectMake(100.0, 100.0, stackViewWidth, 30.0)];
//    spacerStackView.axis = UILayoutConstraintAxisHorizontal;
//    spacerStackView.alignment = UIStackViewAlignmentCenter;
//    spacerStackView.distribution = UIStackViewDistributionFill;
//    spacerStackView.spacing = 0.0;
//    
//    [self.view addSubview:spacerStackView];
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    
    // create a uiview the same size and location as the stack view
    UIView *holderView = [[UIView alloc] initWithFrame:self.myHorStackView.frame];
    holderView.clipsToBounds = YES;
    
    // copy the labels into the uiview
    for (UILabel *eachLabel in self.myHorStackView.arrangedSubviews) {
        // create new label
        UILabel *copyLabel = [[UILabel alloc] initWithFrame:eachLabel.frame];
        copyLabel.font = [UIFont fontWithName:@"TradeGothic-BoldCondTwenty" size:30.0];
        copyLabel.text = eachLabel.text;
        copyLabel.textColor = [UIColor clearColor];
        
        // add new label to array for reference
        [self.myCopyLabels addObject:copyLabel];
        
        // add new label to holderview
        [holderView addSubview:copyLabel];
        
//        NSLog(@"center.x OF %@ = %g", copyLabel.text, copyLabel.center.x);
        // NSLog(@"width OF %@ = %g", copyLabel.text, copyLabel.frame.size.width);
        NSLog(@"height OF %@ = %g", copyLabel.text, copyLabel.frame.size.height);
    }
    
    // remove the stackview
    self.myHorStackView = nil;
    
    // add new holder view to self
    [self.view addSubview:holderView];
    

}


- (IBAction)animateButtonTapped:(id)sender {

    self.labelToMove.center = CGPointMake(15.0, 70.0);
    
    self.isUp = YES;

    for (UILabel *eachLabel in self.myCopyLabels) {
        if (![eachLabel.text isEqualToString:@" "]) {
            if (self.isUp) {
                eachLabel.center = CGPointMake(eachLabel.center.x, eachLabel.center.y + 40.0);
            } else {
                eachLabel.center = CGPointMake(eachLabel.center.x, eachLabel.center.y - 40.0);
            }
            self.isUp = !self.isUp;
            eachLabel.textColor = [UIColor whiteColor];
        }
    }
    
    
    [UIView animateWithDuration:1.0 animations:^{
        self.labelToMove.center = CGPointMake(self.labelToMove.center.x, self.labelToMove.center.y - 40.0);
    }];
    
    self.isUp = YES;
    
    for (UILabel *eachLabel in self.myCopyLabels) {
        if (![eachLabel.text isEqualToString:@" "]) {
            [UIView animateWithDuration:1.0 animations:^{
                if (self.isUp) {
                    eachLabel.center = CGPointMake(eachLabel.center.x, eachLabel.center.y - 40.0);
                } else {
                    eachLabel.center = CGPointMake(eachLabel.center.x, eachLabel.center.y + 40.0);
                }
            }];
            self.isUp = !self.isUp;
        }
    }

    
}


- (void)createLabelsForString:(NSString *)inputString {
    
    for (NSString *eachCharString in [self lettersArrayFromString:inputString]) {
        
        // put each letter in a label
        UILabel *letterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0, 0.0, 15.0, 28.0)];
        letterLabel.font = [UIFont fontWithName:@"TradeGothic-BoldCondTwenty" size:30.0];
        letterLabel.textColor = [UIColor clearColor];
        //letterLabel.backgroundColor = [UIColor grayColor];
        letterLabel.text = eachCharString;
        
        [self.myHorStackView addArrangedSubview:letterLabel];
        
        // add each label into an array
        [self.labelsArray addObject:letterLabel];
    }
}

- (NSArray *)lettersArrayFromString:(NSString *)inputString {
    
    NSMutableArray *arrayOfCharactersInInputString = [NSMutableArray array];
    
    for (int i=0; i<inputString.length; i++)
    {
        NSString *eachCharacterString =[NSString stringWithFormat:@"%C",[inputString characterAtIndex:i]];
        [arrayOfCharactersInInputString addObject:eachCharacterString];
    }
    
    return arrayOfCharactersInInputString;
}


@end
