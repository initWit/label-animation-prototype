//
//  main.m
//  LabelMask
//
//  Created by Robert Figueras on 4/1/16.
//  Copyright © 2016 Robert Figueras. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
